<?php 
    $logo_principal = get_field('logo_principal_mobile', 'option');
?>

<nav class="mobile-nav-wrap hide-desktop" role="navigation">
    <div class="logo">
        <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name');?>" alt="<?php bloginfo('name');?>">
            <img src="<?php if ($logo_principal != '') {echo $logo_principal;} else {echo get_template_directory_uri() . '/build/imgs/demo/logo.png';}?>"
                alt="<?php bloginfo('name');?>" />
        </a>
    </div>
    <div class="search-form">
            <i class="icon icon-lupa"></i><?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
    </div>
    <!--<?php// get_template_part('template_part/components/simple-search'); ?>-->
    <div class="js-menu menu-toggle">
        <div class="hamburger-menu" id="burger"></div>
    </div>
</nav>

<menu class="menu hide-desktop">
    <nav class="bg-mobile-1">
        <div class="logo float-menu">
            <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name');?>" alt="<?php bloginfo('name');?>">
                <img src="<?php if (get_field('logo_principal', 'option') != '') {echo get_field('logo_principal', 'option');} else {echo get_template_directory_uri() . '/build/imgs/demo/logo.png';}?>"
                    alt="<?php bloginfo('name');?>" />
            </a>
        </div>
        <div class="hamburger-menu animate float-menu"></div>
        <ul class="first-level">
            <?php
                $defaults = array(
                            'theme_location' => 'principal',
                            'container' => 'div',
                            'container_class' => 'menu-principal',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'items_wrap' => '<li id="%1$s" class="%2$s">%3$s</li>',
                            'depth' => 0,
                        );
                    wp_nav_menu($defaults);
                ?>
        </ul>
        <span class="title">Cadastre-se na nossa newsletter</span>
        <?php echo do_shortcode('[contact-form-7 id="721" title="cta footer_moby"]'); ?>
        <div class="two_in">
            <div>
                <ul>
                    <li>
                        <h4>Sede Administrativa</h4>
                    </li>
                    <li>
                        <p>MG (31) 3063-0081</p>
                    </li>
                </ul>
            </div>
            <div>
                <ul>
                    <li>
                        <h4>Central de Vendas</h4>
                    </li>
                    <li>
                        <p>0800 944 0081</p>
                    </li>
                </ul>
            </div>
        </div>

        <div class="btn_simulation">
            <a href="<?php  ?>"></a>
        </div>
    </nav>
</menu>