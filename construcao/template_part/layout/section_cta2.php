    <?php 
        if( have_rows('campo_formulario', 'options') ):
        while( have_rows('campo_formulario', 'options') ): the_row(); 
    ?>
        <style>
            .form_call_cta.bg_{
                background:url(<?php echo get_sub_field('imagem_background');?>);
            }

            .icon.icon_cta_certify{
                background:url(<?php echo get_sub_field('imagem_certificado');?>);
            }
        </style>
    <?php 
        endwhile; 
        endif; 
    ?>

<section  id="simule" class="form_call_cta bg_">
    <div class="form_space">
        <div class="title_form_call">
            <h3>
                Simule seu financiamento e descubra quanto você tem de desconto
            </h3>
            <p>Preencha os dados abaixo e ligamos para você:</p>
        </div>

        <div class="form_controller">
            <?= do_shortcode('[contact-form-7 id="121" title="cta_section_topo"]');?>
        </div>
        
        <div class="certify_pos">
            <i class="icon icon_cta_certify"></i>
        </div>

        <a href="<?php echo get_site_url().'/minha-casa-minha-vida'; ?>">Ver mais informações sobre Minha Casa Minha Vida</a>
    </div>
</section>