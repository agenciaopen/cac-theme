    <?php 
        if( have_rows('campo_formulario', 'options') ):
        while( have_rows('campo_formulario', 'options') ): the_row(); 
    ?>
        <style>
            .form_call_cta.bg_{
                background:url(<?php echo get_sub_field('imagem_background');?>);
            }

            .icon.icon_cta_certify{
                background:url(<?php echo get_sub_field('imagem_certificado');?>);
            }
        </style>
    <?php 
        endwhile; 
        endif; 
    ?>

<section  id="simule" class="form_call_cta bg_">
    <div class="form_space">
        <div class="title_form_call">
            <h3>
                Simule seu financiamento e descubra quanto você tem de desconto
            </h3>
            <p>Preencha os dados abaixo e ligamos para você:</p>
        </div>

        <div class="form_controller">
            
            <!--//do_shortcode('[contact-form-7 id="121" title="cta_section_topo"]'); -->
            <?= do_shortcode('[contact-form-7 id="4599" title="cta_section_topo_final"]');?>

        </div>
        
        <div class="certify_pos">
            <i class="icon icon_cta_certify"></i>
        </div>

        <a href="<?php echo get_site_url().'/minha-casa-minha-vida'; ?>">Ver mais informações sobre Minha Casa Minha Vida</a>
    </div>
</section>


<script language="javascript">
(function ($) {
    document.addEventListener( 'wpcf7submit', function( event ) {
            let telefoneCompleto = $( "input[name=mask-496]" ).val();
           console.log(telefoneCompleto);
            let tcn = telefoneCompleto.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').replace(' ', '').trim();
           console.log(tcn);
            let ddd = tcn.substring(0,2);
           console.log(ddd);
            let telefone = tcn.substring(2);
           console.log(telefone);
    if ( '4599' == event.detail.contactFormId ) {
           console.log( "The contact form ID is 4599" );
            // Your code
            let aceite = $( "input[name=acceptance-27]" ).val();
           console.log(aceite);
            let nome = $( "input[name=nome_cta_topo]" ).val();
           console.log(nome);
            let email = $( "input[name=email_cta_topo]" ).val();
           console.log(email);
        
            let mensagem = "";
           console.log(mensagem);
            let cpf = "";
           console.log(cpf);
           setTimeout(function() {

            if((aceite !== undefined && aceite !== '') && (nome !== undefined && nome !== '') && (email !== undefined &&  email !=='') && (ddd !== undefined &&  ddd !=='') && (telefone !== undefined &&  telefone !=='')) {
               console.log('preenchidos');
                //chama();
                var hc_dominio_chat="//cacengenharia.housecrm.com.br"; 
                var hc_https = 1;
                chama();
                function chama(){
                    // let empreendimento = $( "input[type=checkbox][name=bar]:checked" ).val();
                    let empreendimento = '39477';
                    console.log(empreendimento);
                    var ret = hc_envia_mensagem(empreendimento, aceite, nome, email,ddd, telefone,mensagem,cpf);
                    console.log(ret);
                }
                location = '/obrigado';
            }
            else{ 
               console.log("campos vazios");
            }
           }, 2000);
    }
}, false );

})(jQuery);

</script>
