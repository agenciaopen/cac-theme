

<section class="search-imoveis">
        <h3><?php echo get_field('filtro_texto_titulo', 'options'); ?></h3>
        <p>Encontre seu imóvel</p>
        <div class="formulario">
            <?= do_shortcode('[searchandfilter slug="imoveis-2"]'); ?>
        </div>
</section>


<?php if(is_home()){ ?>
        <div class="lance-title">
            <h3>Seu imóvel está aqui <span><i class="icon icon-down-arrow"></i></span></h3>
        </div>    
<?php } else {}?>

<?php if(is_page_template()){ ?>
    <section class="swiper-container launch">
        <div class="lance-title">
            <h3>Seu imóvel está aqui <span>Lançamentos <i class="icon icon-down-arrow"></i></span></h3>
        </div>

        <?php if(is_page_template()){ get_template_part('/template_part/components/show_launch'); }else{} ?>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </section>
<?php }  else {} ?>


<?php if(is_page_template()){ get_template_part('/template_part/components/tab_info'); }else{} ?>

<style>
    <?php
        if ( $imv_posts ) { 
            foreach ( $imv_posts as $post ) : setup_postdata( $post ); 
                $theid = get_the_ID();
            ?>
                .image_results_controller._<?php echo $theid; ?>{
                    background-image:url(<?= the_post_thumbnail_url(); ?>);
                }    
    <?php 
        endforeach;
      }
    ?>
</style>

<?php 
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$imv_posts = get_posts( array(
        'post_type'		 => 'imoveis',
		'posts_per_page' => -1,
        'paged' => $paged,
        )
    );
            
    if ( $imv_posts ) { ?>
    <section id="if_moby">
        <section class="results">
                <div id="loop" class="content-loop">
                        <?php
                                foreach ( $imv_posts as $post ) : 
                                    setup_postdata( $post ); 
                                    $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
                                    $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
                                    $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                                    $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
                                    $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
                                    $icons = get_field('icones_blocos', 'option');
                                    if($fase_da_obra[0] == "Lançamento"){$fase_da_obra[0] = "Lancamento";}else {};
                        ?>          
                        <div id="loop-emp" class="results-list">
                            <div class=" search-filter-result-item post">
                            <a href="<?php the_permalink(); ?>">
                                    <div class="image_results_controller _<?php echo $theid; ?>"></div>
                                    <?php if($cidade[0] && $estado[0]) {?>
                                    <div class="result_content_controller">
                                        <?php if(get_field('residencial')){ ?><p><?php echo get_field('residencial') ?></p><?php }else{} ?>
                                        <?php if($cidade[0] && $estado[0]) { ?><h3><?= $cidade[0]; ?><span><?= $estado[0]; ?></span></h3> <?php } else {} ?>
                                        <?php if(get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?></p><?php } else {} ?>
                                        <?php if($fase_da_obra[0] or get_field('itbi_info')){?>
                                            <div class="tags">
                                                <?php if($fase_da_obra[0]) { ?><p class="tag on <?= $fase_da_obra[0] ?>"><?php if($fase_da_obra[0] == "Lancamento"){echo $fase_da_obra[0] = "Lançamento";}else { echo $fase_da_obra[0]; }; ?></p> <?php } else {} ?>
                                                <?php if(get_field('itbi_info')) { ?><p class="tag off <?php if( get_field('itbi_info') == "Documentação Grátis" ) { echo "Documentacao_gratis"; } else {  echo get_field('itbi_info');  } ?>"><?php echo get_field('itbi_info'); ?></p> <?php } else {} ?>
                                            </div>
                                        <?php }else{} ?>
                                    
                                        <?php
                                            if($quarto[0]){
                                            while( have_rows('icones_blocos', 'options') ): the_row(); 
                                        ?>
                                        <style>
                                            .icon.icon-moby{
                                                background-image:url(<?php echo get_sub_field('quartos_icon')['url'];?>);
                                            }
                                            .icon.icon-req{
                                                background-image:url(<?php echo get_sub_field('medidas_icon')['url'];?>);
                                            }
                                        </style>
                                        <?php
                                            endwhile;
                                        }else{}
                                        ?>

                                        <?php if($quarto[0]){?><p><i class="icon icon-moby"></i><?= $quarto[0]; ?> </p> <?php } else {} ?>

                                        <?php if(get_field('medidas') || get_field('medidas_2')){ ?>
                                            <div class="medidas">
                                                <p class="m2"><i class="icon icon-req"></i><?php echo get_field('medidas') ?> m</p>
                                                <?php if(get_field('medidas_2')){?><span class="m2 left">a <?php echo get_field('medidas_2') ?> m</span><?php }else{}?> 
                                            </div>
                                        <?php } else {}?>
                                    </div>
                                        <?php } else { echo "preencha, cidade e estado."; } ?>
                                </a>
                            </div>
                        </div>

                <?php 
                    endforeach;
                ?>

                <div id="pagination">
                </div>

                <?php
                    wp_reset_postdata();
                ?>
            </div>
    </section> 
    </section>

<?php
    }
?>