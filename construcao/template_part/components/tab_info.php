<?php $information = get_field('tab_info', 'option'); ?>

<?php if( have_rows('tab_info', 'options') ): ?>
    <section id="info_tab" class="info_tab">
    <ul class="inline">
    <?php while( have_rows('tab_info', 'options') ): the_row(); 
    ?>
                <style>
                    .icon.whatsapp{
                        background-image:url(<?php echo get_sub_field('whatsapp_icon')['url'];?>);
                    }
                    .icon.call-to-you{
                        background-image:url(<?php echo get_sub_field('ligamos_icon')['url'];?>);
                    }
                    .icon.call-to-us{
                        background-image:url(<?php echo get_sub_field('vendas_icon')['url'];?>);
                    }
                    .icon.chat-online{
                        background-image:url(<?php echo get_sub_field('chat_icon')['url'];?>);
                    }
                </style>

                <li>
                    <a href="https://api.whatsapp.com/send?1=pt_BR&phone=5531971014425" target="blank">
                        <i class="icon whatsapp"></i>
                        <ul class="no-inline">
                            <li><span><?php if(get_sub_field('atendimento_titulo')){echo get_sub_field('atendimento_titulo');}else{echo 'Atedimento';}?></span></li>
                            <li><h4><?php if(get_sub_field('atendimento_subtitulo')){echo get_sub_field('atendimento_subtitulo');}else{echo 'Whatsapp';}?></h4></li>
                        </ul>   
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url()."/contato#call"; ?>">
                    <i class="icon call-to-you"></i>
                    <ul class="no-inline">
                        <li><span><?php if(get_sub_field('ligamos_titulo')){echo get_sub_field('ligamos_titulo');}else{echo 'Ligamos';}?></span></li>
                        <li><h4><?php if(get_sub_field('ligamos_subtitulo')){echo get_sub_field('ligamos_subtitulo');}else{echo 'Para você';}?></h4></li>
                    </ul>  
                    </a> 
                </li>
                <li>
                    <!--<a href="<?php //echo site_url()."/contato#call"; ?>">-->
                    <a href="tel:0800-944-0081">
                    <i class="icon call-to-us"></i>
                    <ul class="no-inline">
                        <li><span><?php if(get_sub_field('vendas_titulo')){echo get_sub_field('vendas_titulo');}else{echo 'Central de vendas';}?></span></li>
                        <li><h4><?php if(get_sub_field('vendas_subtitulo')){echo get_sub_field('vendas_subtitulo');}else{echo '0800 944 0081';}?></h4></li>
                    </ul> 
                    </a>  
                </li>
                <li>
                    <a href="https://cacengenharia.housecrm.com.br/chat/?empreendimento=&nome=&email=&ddd=&telefone=&pro=1&filial=&source=organic&media=google&campaign=&referrer=https://www.google.com/&keyword=&host=www.cacengenharia.com.br&googleid=685916214.1578406093&enterlink=https://www.cacengenharia.com.br/&informacao=undefined&campanha=&utmcontent=">
                    <i class="icon chat-online"></i>
                    <ul class="no-inline">
                        <li><span><?php if(get_sub_field('chat_titulo')){echo get_sub_field('chat_titulo');}else{echo 'Atendimento';}?></span></li>
                        <li><h4><?php if(get_sub_field('chat_subtitulo')){echo get_sub_field('chat_subtitulo');}else{echo 'Online de Vendas';}?></h4></li>
                        <li><span><?php if(get_sub_field('chat_horario')){echo get_sub_field('chat_horario');}else{echo '8h30 às 18h31';}?></span></li>
                    </ul>   
                    </a>
                </li>
    <?php endwhile; ?>
    </ul>
</section>
<?php endif; ?>


