<section class="has-container mg-r">
    <div class="gallery_and_technical">
        <div class="two_in">
            <div class="fifty">
                <h3>Galeria de imagem</h3>
                <div class="tabs">
                    <div id="icetab-container">
                        <?php if(get_field('empreendimento_ativo')) { ?> <div class="icetab current-tab">Empreendimento
                        </div> <?php }else{} ?>
                        <?php if(get_field('decorado_ativo')) { ?> <div class="icetab">Decorado</div> <?php }else{} ?>
                        <?php if(get_field('plantas_ativo')) { ?> <div class="icetab">Plantas</div> <?php }else{} ?>
                    </div>
                    <div id="icetab-content">
                        <?php  if(get_field('empreendimento_ativo')){ ?>
                        <div class="tabcontent tab-active">
                            <div class="swiper-container gallery-top-1">
                                <div class="swiper-wrapper">
                                    <?php 
 
                                $get_gallery = get_field('galeria_empreendimento'); 
                                foreach($get_gallery as $img){
                            ?>
                                    <div class="swiper-slide" style="background-image:url(<?php echo $img; ?>);"></div>
                                    <?php
                                }
                            ?>
                                </div>
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"></div>
                            </div>

                            <div class="swiper-container gallery-thumbs-1">
                                <div class="swiper-wrapper">
                                    <?php 
                                $get_gallery = get_field('galeria_empreendimento'); 
                                foreach($get_gallery as $img){
                                ?>
                                    <div class="swiper-slide" style="background-image:url(<?php echo $img; ?>);"></div>
                                    <?php
                                }
                            ?>
                                </div>
                            </div>
                        </div>
                        <?php }else{} ?>
                        <?php if(get_field('decorado_ativo')){ ?>
                        <div class="tabcontent">
                            <div class="swiper-container gallery-top-2">
                                <div class="swiper-wrapper">
                                    <?php 
 
                                $get_gallery = get_field('galeria_decorado'); 
                                foreach($get_gallery as $img){
                            ?>
                                    <div class="swiper-slide" style="background-image:url(<?php echo $img; ?>);"></div>
                                    <?php
                                }
                            ?>
                                </div>
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"></div>
                            </div>

                            <div class="swiper-container gallery-thumbs-2">
                                <div class="swiper-wrapper">
                                    <?php 
                                $get_gallery = get_field('galeria_decorado');  
                                foreach($get_gallery as $img){
                                ?>
                                    <div class="swiper-slide" style="background-image:url(<?php echo $img; ?>);"></div>
                                    <?php
                                }
                            ?>
                                </div>
                            </div>
                        </div>
                        <?php }else{} ?>
                        <?php  if(get_field('plantas_ativo')){ ?>
                        <div class="tabcontent">
                            <div class="swiper-container gallery-top-3">
                                <div class="swiper-wrapper">
                                    <?php 
 
                                $get_gallery = get_field('galeria_plantas'); 
                                foreach($get_gallery as $img){
                            ?>
                                    <div class="swiper-slide" style="background-image:url(<?php echo $img; ?>);"></div>
                                    <?php
                                }
                            ?>
                                </div>
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"></div>
                            </div>

                            <div class="swiper-container gallery-thumbs-3">
                                <div class="swiper-wrapper">
                                    <?php 
                                $get_gallery = get_field('galeria_plantas'); 
                                foreach($get_gallery as $img){
                                ?>
                                    <div class="swiper-slide" style="background-image:url(<?php echo $img; ?>);"></div>
                                    <?php
                                }
                            ?>
                                </div>
                            </div>
                        </div>
                        <?php }else{} ?>
                    </div>
                </div>
            </div>
            <div class="fifty">
                <div class="datasheet_controller">
                    <div class="datasheet_content">
                        <h3>Ficha Técnica</h3>
                        <ul class="datasheet_red_bubble">
                            <?php if( have_rows('ficha_tecnica') ): ?>
                            <?php while( have_rows('ficha_tecnica') ): the_row(); ?>
                            <?php if(get_sub_field('produto')){ ?><li>Produto: <?php echo get_sub_field('produto'); ?>
                            </li><?php } ?>
                            <?php if(get_sub_field('tipologia_da_unidade')){?><li>Tipologia da unidade:
                                <?php echo get_sub_field('tipologia_da_unidade'); ?></li><?php } ?>
                            <?php if(get_sub_field('proximidade')){?><li>Proximidade:
                                <?php echo get_sub_field('proximidade'); ?></li><?php } ?>
                            <?php if(get_sub_field('realizacao')){?><li>Realização:
                                <?php echo get_sub_field('realizacao'); ?></li><?php } ?>
                            <?php if(get_sub_field('registro_de_incorporacao')){?><li>Registro de incorporação:
                                <?php echo get_sub_field('registro_de_incorporacao'); ?></li><?php } ?>
                            <br>
                            <li>
                                <a class="btn_red" href="#sabermais">
                                    Quero saber mais!
                                </a>
                            </li>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>