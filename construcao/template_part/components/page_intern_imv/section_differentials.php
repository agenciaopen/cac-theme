<section class="has-container-right">
    <div class="differentials_imv">
        <div class="two_in eighty">
            <h3 class="title_about_imv">Diferenciais</h3>
            <div class="differentials_controller">
                <ul class="icons_presentation">
                <style>
	                <?php 
	                    $counter = 1;
	                    if( have_rows('diferenciais_icones', 'options') ):
	                    while( have_rows('diferenciais_icones', 'options') ): the_row(); 
	                ?>
	                        .differentials_imv ul.icons_presentation li .icon_controller i.icon_<?php echo $counter++; ?>{
	                            background-image:url(<?php echo get_sub_field('icones_diferenciais');?>);
	                        }
	                <?php 
	                    endwhile; 
	                    endif; 
                ?>
                </style>
                    <?php
                         error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
                        if(get_field('diferenciais') != ''){
                        $check_field = get_field('diferenciais');
                        //print_r($check_field);
                        //var_dump($check_field);
                        //$check_field = ($post->ID, 'estado', array("fields" => "names"));
                        $counter = 0;
                        foreach($check_field as $field){
                            if($field['value'] == 'Selecionar Tudo'){
                                $counter = 0;
                            } else if($field['value'] == 'Centro Comercial'){
                                $counter = 1;
                            } else if ($field['value'] == 'Quadra Poliesportiva'){
                                $counter = 2;
                            } else if ($field['value'] == 'Playground'){
                                $counter = 3;
                            } else if ($field['value'] == 'Salão de Festa'){
                                $counter = 4;
                            } else if ($field['value'] == 'Piscina'){
                                $counter = 5;
                            } else if ($field['value'] == 'Bicicletário'){
                                $counter = 6;
                            } else if ($field['value'] == 'Segurança'){
                                $counter = 7;
                            } else if ($field['value'] == 'Quartos com varanda'){
                                $counter = 8;
                            } else if ($field['value'] == 'Vaga de Estacionamento'){
                                $counter = 9;
                            } else if ($field['value'] == 'Acesso PNE'){
                                $counter = 10;
                            } else if($field['value'] == 'Complexo Aquático') {
                                $counter = 11;
                            } else if($field['value'] == 'Cinema') {
                                $counter = 12;
                            } else if($field['value'] == 'Academia') {
                                $counter = 13;
                            } else if($field['value'] == 'Espaço Fitness') {
                                $counter = 14;
                            } else if($field['value'] == 'Churrasqueira') {
                                $counter = 15;
                            }  else if($field['value'] == 'Salão de Jogos') {
                                $counter = 16;
                            }  else if($field['value'] == 'Car Wash') {
                                $counter = 17;
                            }  else if($field['value'] == 'Oficina') {
                                $counter = 18;
                            }  else if($field['value'] == 'Espaço Gourmet') {
                                $counter = 19;
                            }  else if($field['value'] == 'Pista de Skate') {
                                $counter = 20;
                            } else if($field['value'] == 'Pet Place') {
                                $counter = 21;
                            } 
                             else {
                  				$counter++;
                            }
                            
                    ?>
                        <li><div class="icon_controller"><i class="icon_<?php echo $counter;?> "></i><p><?php echo $field['value']; ?></p></div></li>
                    <?php
                    }
                    }else{}?>
                </ul>
                <div class="btn_simulacao">
                    <a href="<?php echo get_field('botao_simulacao_link'); ?>">
                        <img src="<?php echo get_field('botao_simulacao'); ?>" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="two_in twenty">
            <div class="bg_collumn_right">     
                <img src="<?php echo get_field('banner_diferenciais'); ?>" alt="">               
            </div>
        </div>
    </div>
</section>