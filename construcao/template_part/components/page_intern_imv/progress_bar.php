<?php if(get_field('status_ativo') == true) { ?> 
    <section class="full-container">
        <div class="progress_controller">
            <h3>Status do empreendimento</h3>
        <?php
                            if(get_field('status') != ''){
                            $status_field = get_field('status');
                            $counter = 0;
                            foreach($status_field as $field){
                                if($field == 'Breve Lançamento'){
                                    $counter = 16.66;
                                } else if ($field == 'Lançamento'){
                                    $counter = 33.32;
                                } else if ($field == 'Obras iniciadas'){
                                    $counter = 49.98;
                                } else if ($field == 'Obras avançadas'){
                                    $counter = 66.64;
                                } else if ($field == 'Pronto para morar'){
                                    $counter = 83.3;
                                } else if ($field == 'Entregue'){
                                    $counter = 100;
                                } else {
                                    $counter = 0;
                                }
                            }
                        }else{}?>

            <div class="progress-wrap progress" data-progress-percent="<?php echo $counter; ?>">
                <div class="progress-bar progress"></div>
            </div>
            <ul>
                <li class="<?php if($counter == 20){ echo "active"; } ?>">Breve Lançamento</li>
                <li class="<?php if($counter == 40){ echo "active"; } ?>">Lançamento</li>
                <li class="<?php if($counter == 60){ echo "active"; } ?>">Obras iniciadas</li>
                <li class="<?php if($counter == 60){ echo "active"; } ?>">Obras avançadas</li>
                <li class="<?php if($counter == 80){ echo "active"; } ?>">Pronto para morar</li>
                <li class="<?php if($counter == 100){ echo "active"; } ?>">Entregue</li>
            </ul>
        </div>
    </section>    
<?php }else{} ?>


