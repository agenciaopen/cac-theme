<section class="section-icetab">
    <h3>Escolha um de nossos formulários para entrar em contato:</h3>
    <div id="icetab-container">
        <div class="icetab current-tab">Cliente</div><div class="icetab">Fornecedor</div><div class="icetab">Trabalhe conosco</div><div class="icetab">Venda seu terreno</div>       
    </div>
        
    <div id="icetab-content">
        <div id="call" class="tabcontent tab-active">
            <h3>Esse é um canal direto com a C.A.C.</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="120" title="Cliente"]'); ?>
        </div> 
        <div class="tabcontent">
            <h3>Seja um fornecedor também</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="358" title="Fornecedor"]'); ?>
        </div>
        <div class="tabcontent">
            <h3>Faça parte da equipe C.A.C Engenharia.</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="359" title="Trabalhe conosco"]'); ?>
        </div>
        <div class="tabcontent">
            <h3>Preencha as informações abaixo. <br> Em breve entraremos em contato.</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="360" title="Venda seu terreno"]'); ?>
        </div>
    </div> 
</section>

