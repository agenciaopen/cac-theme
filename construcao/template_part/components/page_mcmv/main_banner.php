<section class="swiper-container banner">
    <div class="swiper-wrapper banner_controller">    

        <?php
                    global $post;
                    $myposts = get_posts( array(
                            'posts_per_page' => -1,
                            'offset'         => 0,
                            'post_type'		 => 'page',
                            //'post_name' => 'sobre-a-c-a-c'
                        ) 
                    );
                            
                    if ( $myposts ) {
                    foreach ( $myposts as $post ) : 
                            setup_postdata( $post );

                            //print_r($post);
                            $the_id = 190;
                            $banner_principal = get_field('banner_esquerda', $the_id);
                            $banner_mobile = get_field('banner_mobile_mcmv', $the_id);
                            $banner_certificado = get_field('botao_certificado', $the_id);
                            $banner_text = get_field('texto_lateral', $the_id);
                            $post_id = get_the_ID();
                            
                            if($post_id == 190){
        ?>
        
    
                        <style>
                            .bg._left.<?php echo "_identi_".$post_id ?>{
                                background-image: url('<?php echo $banner_principal; ?>');
                            }
                            .bg._left.<?php echo "_identi_".$post_id ?>.moby {
                                background-image: url('<?php echo $banner_mobile; ?>');
                            } 
                            .bg._certify.<?php echo "_identi_".$post_id ?>{
                                background-image: url('<?php echo $banner_certificado; ?>');
                            }
                        </style>
            
            <div class="swiper-slide bg_ ">
                <a href="<?php echo get_field('imagem_certificado_link', 112); ?>">

                            <div class="bg  _left <?php echo "_identi_".$post_id ?>">
                                        <div class="bg  _certify <?php echo "_identi_".$post_id ?>"></div>
                            </div>

                </a>
                </div>
            <?php
                } else {}
                endforeach;
                    wp_reset_postdata();
                }
            ?>
    </div>
    
</section>
    
    
    