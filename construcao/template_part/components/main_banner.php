<div class="swiper-wrapper banner_controller">

    <?php
			global $post;
			$myposts = get_posts( array(
					'posts_per_page' => -1,
					'offset'         => 0,
					'post_type'		 => 'banner',
				) 
			);
					
			if ( $myposts ) {
			foreach ( $myposts as $post ) : 
                    setup_postdata( $post ); 
                    $banner_principal = get_field('banner_direita');
                    $banner_mobile = get_field('banner_mobile_h');
                    $banner_certificado = get_field('banner_esquerda_certificado');
                    $banner_text = get_field('texto_banner_lateral');
                    $post_id = get_the_ID();
                    $count = wp_count_posts();
?>


    <style>
    .bg._left.<?php echo "_identi_".$post_id ?> {
        background-image: url('<?php echo $banner_principal; ?>');
    }

    .bg._left.<?php echo "_identi_".$post_id ?>.moby {
        background-image: url('<?php echo $banner_mobile; ?>');
    } 

    .bg._certify.<?php echo "_identi_".$post_id ?> {
        background-image: url('<?php echo $banner_certificado; ?>');
    }
    </style>

    <div class="swiper-slide bg_ ">
        <a href="<?php echo get_field('imagem_certificado_link'); ?>">
            <div id="bg_changer_<?php echo $post_id; ?>" class="bg  _left <?php echo "_identi_".$post_id ?>">
                    <div class="bg  _certify <?php echo "_identi_".$post_id ?>"></div>
            </div>
        </a>
    </div>

    <?php
			endforeach;
				wp_reset_postdata();
			}
		?>
</div>
<?php if(is_home()){
    if( $count->publish > 1){
        echo '<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>';
        echo '<div class="swiper-pagination"></div>';
    } else{
        echo '<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>';
        echo '<div class="swiper-pagination"></div>';
    }
} else {} ?>