<section class="about_presentation">
    <div class="about_controller">
        <div class="two_in">
            <div class="about_inside">
                <h3>Sobre a C.A.C</h3>
                <p><?php echo get_field('sobre_a_cac') ?></p>
            </div>
            <div class="static_map bg_">               
                <h3>Áreas de Atuação Nacional</h3>
                <ul class="list_style_red">
                    <li><h5>Minas Gerais</h5></li>
                    <li><h5>Rio de Janeiro</h5></li>
                    <li><h5>São Paulo</h5></li>
                </ul>
            </div>
        </div>
    </div>
</section>