const { src, dest, watch, series, parallel } = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const browserSync = require('browser-sync').create();
var replace = require('gulp-replace');

const files = { 
    scssPath: './assets/sass/**/*.scss',
    jsPath: './assets/js/**/*.js'
}


function scssTask(){    
    return src(files.scssPath)
        .pipe(sourcemaps.init()) 
        .pipe(sass())
        .pipe(postcss([ autoprefixer(), cssnano() ])) 
        .pipe(sourcemaps.write('.min'))
		.pipe(dest('build/css')
		.pipe(browserSync.stream())
    );
}


function jsTask(){
    return src([
        files.jsPath
        ])
        .pipe(concat('all.js'))
        .pipe(uglify())
		.pipe(dest('build/js')
		.pipe(browserSync.stream())
    );
}

function watchTask(){
	browserSync.init({
		proxy: "http://localhost/CAC-engenharia/",
    }),
    watch([files.scssPath, files.jsPath], 
        series(
            parallel(scssTask, jsTask)
        )
    ).on('change',browserSync.reload);    
}

exports.default = series(
    parallel(scssTask, jsTask), 
    watchTask
);