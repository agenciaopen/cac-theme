<?php 
/*
* Template Name: Contato
* Template para página de Contato
*/
get_header();

    get_template_part('template_part/components/tab_info');
    //search imoveis

    get_template_part('template_part/components/page_contact/main_banner');
    get_template_part('template_part/components/page_contact/section_contact');
    ?>
    <section class="full-container-no-bg">
        <h4>Veja outros imóveis semelhantes a esse!</h4>
        <div class="swiper-container launch">
            <?php get_template_part('/template_part/components/show_launch'); ?>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>

    <?php

get_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri().'/build/js/scripts.min.js'; ?>"></script> 


