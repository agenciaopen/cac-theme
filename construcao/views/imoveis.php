<?php 
/*
* Template Name: Imóveis
* Template para página de imóveis
*/
get_header();

	//fixed imoveis
 	get_template_part('template_part/layout/imoveis-fixed-with-pagination'); 

	//section cta
	get_template_part('template_part/layout/section_cta');

get_footer(); ?>