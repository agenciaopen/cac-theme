<?php 
/*
* Template Name: MCMV
* Template para página de Minha casa minha vida
*/
get_header();

    get_template_part('template_part/components/tab_info');
    //search imoveis
    get_template_part('template_part/components/page_mcmv/main_banner');
    get_template_part('template_part/components/search-imoveis');
        //components page
        get_template_part('template_part/components/page_mcmv/section_mcmv');
        get_template_part('template_part/components/page_mcmv/section_how_mcmv');
        get_template_part('template_part/components/page_mcmv/section_info');
    
    ?>
    <section class="cta-container">
       <?php get_template_part('template_part/layout/section_cta'); ?>
    </section>

    <section class="full-container-no-bg">
        <h4>Veja outros imóveis semelhantes a esse!</h4>
        <div class="swiper-container launch">
        <?php get_template_part('/template_part/components/show_launch'); ?>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        </div>
   </section>
    <?php

get_footer(); ?>