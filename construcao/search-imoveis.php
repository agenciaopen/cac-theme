<?php

get_header();

?>

    <section class="busca cf">
        <div class="formulario">
            <?= do_shortcode('[searchandfilter slug="pesquisa-imoveis"]'); ?>
        </div>
    </section>
    <section class="empreendimentos cf">
        <div id="loop-emp" class="content-loop results-list">
            <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                $bairro = wp_get_post_terms($post->ID, 'bairro', array("fields" => "names"));
                $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
            ?>
            <div class=" search-filter-result-item post">
                <a href="<?php the_permalink(); ?>"><h2><?= get_the_title(); ?></h2></a>
                <span> <?= $estado[0]; ?> </span>
                <span> <?= $cidade[0]; ?> </span>
                <span> <?= $bairro[0]; ?> </span>
                <a href="<?php the_permalink(); ?>" class="btn btn-emp">Conheça o empreendimento</a>
            </div>
        <?php endwhile; endif; ?>
        </div>
    </section>
<?php get_footer(); ?>
