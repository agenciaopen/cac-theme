
 	$( window ).resize(function() {
		var now_w = $( window ).width();

		if(now_w >= 1920){
			$('.bg._left').removeClass('moby');
		} else if( now_w <= 768){
			$('.bg._left').addClass('moby');
		}
	});

	$(document).ready(function(){
		var now_w = $( window ).width();

		if(now_w >= 1920){
			$('.bg._left').removeClass('moby');
		} else if( now_w <= 768){
			$('.bg._left').addClass('moby');
		}
	});
 
 
 $(document).ready(function () {
    var banner_principal = new Swiper('.banner', {
        loop:false,
        cssMode: true,
      });
  });
  
  var search_imoveis_ = document.location.href;
  console.log(search_imoveis_);

  if(search_imoveis_.includes('/sobre-a-c-a-c')){
    try{
        //$('.swiper-wrapper.banner_controller').html('');
        //$('.swiper-wrapper').removeClass('banner_controller');
        //$('.swiper-wrapper').removeClass('swiper-wrapper');
        $('.banner-category').html('');
        $('.banner-category').removeClass('banner-category');
    } catch(err){
        console.log('verificador href falhou, home.js - l:44');
    }
  }

  jQuery(document).ready(function($){

    var blocos_imoveis = new Swiper('.results', {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: {
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 0,
				  },
				640: {
				  slidesPerView: 1,
				  slidesPerGroup: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  slidesPerGroup: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  slidesPerGroup: 3,
				  spaceBetween: 45,
				},
				1366: {
					slidesPerView: 4,
					slidesPerGroup: 4,
                    spaceBetween: 45,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
			  el: '.swiper-pagination',
			  clickable: true,
			}
	  });
	  
	  var launch_imoveis = new Swiper('.launch', {
		slidesPerView: 4,
		slidesPerGroup: 4,
		spaceBetween: 45,
		loop:false,
		breakpoints: {
			320: { 
				slidesPerView: 1,
				slidesPerGroup: 1,
				spaceBetween: 0,
				},
			  640: {
				slidesPerView: 1,
				slidesPerGroup: 1,
				spaceBetween: 20,
			  },
			  768: {
				slidesPerView: 2,
				slidesPerGroup: 2,
				spaceBetween: 30,
			  },
			  1024: {
				slidesPerView: 4,
				slidesPerGroup: 4,
				spaceBetween: 16,
			  },
			  1366: {
						  slidesPerView: 4,
						  slidesPerGroup: 4,
						  spaceBetween: 16,
			  },
			  1920: {
						  slidesPerView: 4,
						  slidesPerGroup: 4,
						  spaceBetween: 16,
					  },
		},
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
	  });
  });