$(document).ready(function () {
    var launch_imoveis = new Swiper('.launch', {
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 45,
        loop:false,
        breakpoints: {
            320: { 
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 0,
                },
              640: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 30,
              },
              1024: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 16,
              },
              1366: {
                          slidesPerView: 4,
                          slidesPerGroup: 4,
                          spaceBetween: 16,
              },
              1920: {
                          slidesPerView: 4,
                          slidesPerGroup: 4,
                          spaceBetween: 16,
                      },
        },
        navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
    });
});