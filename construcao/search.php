<?php get_header(); ?>

<h3 class="title-search"><?php echo get_field('filtro_texto_titulo', 'options'); ?></h3>

<section class="search">
    <p>Encontre seu imóvel</p>
    <div class="formulario">
        <?= do_shortcode('[searchandfilter slug="imoveis"]'); ?>
    </div>
</section>

<?php if(is_home()){ get_template_part('/template_part/components/tab_info'); }else{} ?>
<section id="resultados" class="results-fix">
    <div class="content-loop">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php
                $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
                $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
                $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
                $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
                $icons = get_field('icones_blocos', 'option');
            ?>          
            <?php /* Start the Loop */ ?>
            
            
            <div id="loop-emp" class="results-list">
                <div class="search-filter-result-item post">
                    <a href="<?php the_permalink(); ?>">
                        <div class="image_results_controller"> <span class="search-post-thumbnail"><?php the_post_thumbnail('medium');  ?></span></div>
                                <div class="result_content_controller">
                                    <p><span class="search-post-title"><?php the_title(); ?></span></p>
                                    <h3><?php echo $cidade[0]; ?> <span></span> <?php  echo $estado[0]; ?></h3>
                                    <p class=""><?php //echo $fase_da_obra[0]; ?></p>
                                    <p><?php if(get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?></p>
                                        <?php } else {} ?></p>
                                    <p><?php
                                            if($quarto[0]){
                                            while( have_rows('icones_blocos', 'options') ): the_row(); 
                                        ?>

                                        <?php if($fase_da_obra[0] or get_field('itbi_info')){?>
                                        <div class="tags">
                                            <?php if($fase_da_obra[0]) { ?><p class="tag on <?= $fase_da_obra[0] ?>"><?php if($fase_da_obra[0] == "Lancamento"){echo $fase_da_obra[0] = "Lançamento";} else if($fase_da_obra[0] == "Breve_Lancamento"){echo $fase_da_obra[0] = "Breve Lançamento";}  else if($fase_da_obra[0] == "Documentacao_gratis") { $fase_da_obra[0] = "Documentação Grátis";} else { echo $fase_da_obra[0]; }; ?></p> <?php } else {} ?>
                                            <?php if(get_field('itbi_info')) { ?><p
                                                class="tag off <?php if( get_field('itbi_info') == "Documentação Grátis" ) { echo "Documentacao_gratis"; } else {  echo get_field('itbi_info');  } ?>">
                                                <?php echo get_field('itbi_info'); ?></p> <?php } else {} ?>
                                        </div>
                                        <?php }else{} ?>

                                        <style>
                                        .icon.icon-moby {
                                            background-image: url(<?php echo get_sub_field('quartos_icon')['url'];
                                            ?>);
                                        }

                                        .icon.icon-req {
                                            background-image: url(<?php echo get_sub_field('medidas_icon')['url'];
                                            ?>);
                                        }
                                        </style>
                                        <?php
                                                        endwhile;
                                                    }else{}
                                                    ?></p>
                                    <p><img src="<?php echo $icons['quartos_icon']['url'];?>" alt="" class="ico_quarto"><?php echo $quarto[0]; ?> Quartos</p>
                                    

                        </div> 
                    </a>
                </div>
            </div>
            
            <style>
                .bg._left.<?php echo "_identi_".$post_id ?>{
                    background-image: url('<?php echo $banner_principal; ?>');
                }
                .bg._left.<?php echo "_identi_".$post_id ?>.moby {
                    background-image: url('<?php echo $banner_mobile; ?>');
                } 
                .bg._certify.<?php echo "_identi_".$post_id ?>{
                    background-image: url('<?php echo $banner_certificado; ?>');
                }
            </style>
            <?php endwhile; ?>
 
            <?php //the_posts_navigation(); ?>
 
        <?php else : ?>
 
            <?php //get_template_part( 'template-parts/content', 'none' ); ?>
 
        <?php endif; ?>
    </div>
</section><!-- #primary -->

<?php get_footer(); ?>
            <script>
                $( window ).resize(function() {
            var now_w = $( window ).width();

            if(now_w >= 1920){
                $('.bg_').removeClass('moby');
            } else if( now_w <= 768){
                $('.bg_ ').addClass('moby');
            }
            });


            $(document).ready(function(){
            var now_w = $( window ).width();

            if(now_w >= 1920){
                $('.bg_ ').removeClass('moby');
            } else if( now_w <= 768){
                $('.bg_ ').addClass('moby');
            }
            });
            </script>


       
