(function () {

	$( window ).resize(function() {
		var now_w = $( window ).width();

		if(now_w >= 1920){
			$('.bg._left').removeClass('moby');
		} else if( now_w <= 768){
			$('.bg._left').addClass('moby');
		}
	  });

	  $(document).ready(function(){
		var now_w = $( window ).width();

		if(now_w >= 1920){
			$('.bg._left').removeClass('moby');
		} else if( now_w <= 768){
			$('.bg._left').addClass('moby');
		}
	  });

	jQuery(document).ready(function($){
		var $itemForm = $('.searchandfilter ul li');
		$itemForm.first().addClass( "ativo" );

		$( window ).on( "load", function() {
			$('.searchandfilter ul li').each(function (index, value) {
				var $valor = $( this ).find("select").val();
				if ( $valor ){
					$( this ).removeClass('ativo');
					$( this ).next().addClass('ativo');
				}
			});
		});


		$(document).on("sf:ajaxfinish", ".searchandfilter", function(){
			$('.searchandfilter ul li').each(function (index, value) {
				var $valor = $( this ).find("select").val();
				if ( $valor ){
					$( this ).removeClass('ativo');
					$( this ).next().addClass('ativo');
				}
			});
			$('.search-filter-result-item').each(function (index, value) {
				var $valor = $( index + '_posts' );
			});
		});
	});

	$(document).ready(function () {
		var banner_principal = new Swiper('.banner', {
			loop:true,
			direction: 'horizontal',
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			keyboard: true,
		  });

		  var search_imoveis_ = document.location.href;
		  console.log(search_imoveis_);
		  var result_class = ".results";

		  if (search_imoveis_.includes('/r-imoveis')) {
				search_imoveis_ = true;
				try{
					if(search_imoveis_ == true){
						$('head').append('<link rel="stylesheet" href="/wp-content/themes/construcao/build/css/imoveis.min.css"></link>');
						$('#if_moby').addClass('page-template-imoveis');
						$('.search-imoveis').hide();
						$('.launch').removeClass('swiper-container');
						$('.show_fixed_launch').show();
					}

					result_class = '.results-off';
					
				} catch(err){
					console.log('verificador href falhou, home.js - l:44');
				}
		 	} 

		var blocos_imoveis = new Swiper(result_class, {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 16,
			loop:false,
			breakpoints: {
				320: {
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 5,
				  },
				640: {
				  slidesPerView: 1,
				  slidesPerGroup: 1,
				  spaceBetween: 16,
				},
				768: {
				  slidesPerView: 2,
				  slidesPerGroup: 2,
				  spaceBetween: 16,
				},
				1024: {
				  slidesPerView: 3,
				  slidesPerGroup: 3,
				  spaceBetween: 16,
				},
				1366: {
					slidesPerView: 4,
					slidesPerGroup: 4,
                    spaceBetween: 16,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			}
		  });

		  var search_val = $('.result .swiper-slide').length;

		  var search_imoveis = new Swiper('.result' , {
			slidesPerView: search_val,
			slidesPerGroup: search_val,
			spaceBetween: 16,
			loop:false,
			breakpoints: {
				320: {
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 5,
				  },
				640: {
				  slidesPerView: 1,
				  slidesPerGroup: 1,
				  spaceBetween: 16,
				},
				768: {
				  slidesPerView: 2,
				  slidesPerGroup: 2,
				  spaceBetween: 16,
				},
				1024: {
				  slidesPerView: 3,
				  slidesPerGroup: 3,
				  spaceBetween: 16,
				},
				1366: {
					slidesPerView: 4,
					slidesPerGroup: 4,
                    spaceBetween: 16,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev', 
			},
		  });

		  var launch_imoveis = new Swiper('.launch', {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: { 
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 5,
				  },
				640: {
				  slidesPerView: 1,
				  slidesPerGroup: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  slidesPerGroup: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  slidesPerGroup: 3,
				  spaceBetween: 16,
				},
				1366: {
					slidesPerView: 4,
					slidesPerGroup: 4,
                    spaceBetween: 16,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
		  });
	  });  
})();

(function () {
	var div = document.createElement("div");
	function guid() {
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
	}

	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	var user = {
		id: guid().toUpperCase(),
		name: 'User-' + Math.floor((1 + Math.random()) * 10000)
	};
	var botConnection = new BotChat.DirectLine({
		token: 'Aop8xYKmqk8.su4a5OOLO0wFccHElRxrQfuKM98f97DbI0JQOPDx_-4',
		user: user
	});
	
	document.getElementsByTagName('body')[0].appendChild(div);
	div.outerHTML = "<div id='botDiv' style='width: 600px; height: 0px; margin:10px; position: fixed; bottom: 0; right:0; z-index: 1000;><div  id='botTitleBar' style='height: 40px; width: 600px; position:fixed; '></div></div>";
	BotChat.App({
		botConnection: botConnection,
		user: user,	    	
		bot: { id: 'botid', name: 'bot name' },
	resize: 'detect'
	}, 
	document.getElementById("botDiv"));		
	document.getElementsByClassName("wc-header")[0].setAttribute("id", "chatbotheader");
	document.getElementsByClassName("wc-header")[0].innerHTML = "<span>Bate papo!</span>";
	document.getElementsByClassName("wc-header")[0].innerHTML += "<button type='button' onclick='doClose()' style='top: 20%; right:2%; position:absolute; color: #FFFFFF; cursor: pointer;border: none'>X</button>";        
	document.getElementById("mychat").addEventListener("click", function (e) 
	{
	   document.getElementById("botDiv").style.height = '70%';           
	   document.getElementById("botDiv").style.width = '28%';
	   document.getElementById("botDiv").style.minWidth = "345px";
	   e.target.style.display = "none";
	   
	   botConnection
		.postActivity({
			from: user,
			name: 'setUserIdEvent',
			type: 'event',
			value: 'setUserIdEvent'
		})
		.subscribe(function (id) {
			console.log('"trigger setUserIdEvent" sent');
		});        
	})
}());