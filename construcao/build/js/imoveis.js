! function() {
    jQuery(document).ready(function(e) {
        e(".searchandfilter ul li").first().addClass("ativo"), e(window).on("load", function() {
            e(".searchandfilter ul li").each(function(i, s) {
                e(this).find("select").val() && (e(this).removeClass("ativo"), e(this).next().addClass("ativo"))
            })
        }), e(document).on("sf:ajaxfinish", ".searchandfilter", function() {
            e(".searchandfilter ul li").each(function(i, s) {
                e(this).find("select").val() && (e(this).removeClass("ativo"), e(this).next().addClass("ativo"))
            }), e(".search-filter-result-item").each(function(i, s) {
                var n = e(i + "_posts");
                console.log(n.selector)
            })
        })
    }), $(document).ready(function() {
        document.location.href, new Swiper(".launch", {
            slidesPerView: 4,
            slidesPerGroup: 3,
            spaceBetween: 16,
            loop: !1,
			breakpoints: {
				320: { 
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 15,
					},
				  640: {
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 20,
				  },
				  768: {
					slidesPerView: 2,
					slidesPerGroup: 2,
					spaceBetween: 30,
				  },
				  1024: {
					slidesPerView: 4,
					slidesPerGroup: 4,
					spaceBetween: 16,
				  },
				  1366: {
							  slidesPerView: 4,
							  slidesPerGroup: 4,
							  spaceBetween: 16,
				  },
				  1920: {
							  slidesPerView: 4,
							  slidesPerGroup: 4,
							  spaceBetween: 16,
						  },
			},
            navigation: {
                nextEl: ".swiper-button-next .launch",
                prevEl: ".swiper-button-prev .launch"
            }
        }), new Swiper(".results", {
            slidesPerView: 4,
            speed: 800,
            slidesPerColumn: 2,
            slidesPerGroup: 4,
            grabCursor: false,
            spaceBetween: 16,
            slidesPerColumnFill: "row",
			breakpoints: {
				320: { 
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 15,
					},
				  640: {
					slidesPerView: 1,
					slidesPerGroup: 1,
					spaceBetween: 20,
				  },
				  768: {
					slidesPerView: 2,
					slidesPerGroup: 2,
					spaceBetween: 30,
				  },
				  1024: {
					slidesPerView: 4,
					slidesPerGroup: 4,
					spaceBetween: 16,
				  },
				  1366: {
							  slidesPerView: 4,
							  slidesPerGroup: 4,
							  spaceBetween: 16,
				  },
				  1920: {
							  slidesPerView: 4,
							  slidesPerGroup: 4,
							  spaceBetween: 16,
						  },
			},
            navigation: {
                nextEl: ".swiper-button-next.pagination-position-arrow",
                prevEl: ".swiper-button-prev.pagination-position-arrow"
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: !0,
                renderBullet: function(e, i) {
                    return '<span class="' + i + '">' + (e + 1) + "</span>"
                }
            }
        })
    })
}();