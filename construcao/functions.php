<?php

// Plugins
include_once get_template_directory() . '/plugins/breadcrumbs/breadcrumbs.php';

// Adicionar Tamanho de Thumbnail
add_theme_support( 'post-thumbnails' );
add_image_size( 'blog-medium', 700, 600, array( 'center' ) );
add_filter( 'max_srcset_image_width', 'filtered_img' );

function filtered_img(){
	return 1;
}


// Adicionar arquivo por página
function add_file_min(  ){
	global $post;
	if (is_home() || is_front_page()) {
		$file_name = 'home';
	} elseif ( is_page() ){
		// Automatizar por nome do modelo de página
		$pathfile = $post->page_template;
		if ( $pathfile == 'default') {
			$file_name = 'default';
		} else {
			$file_name = substr($pathfile,6,-4);
		}
	} elseif ( is_tax() ) {
		$file_name = get_queried_object()->taxonomy;
	} elseif ( is_category() ) {
		$file_name = 'category';
	} elseif ( is_search() ) {
		$file_name = 'search';
	} elseif ( is_singular() ) {
		$file_name = get_post_type();
	} elseif ( is_attachment() ) {
		$file_name = 'attachment';
	} elseif ( is_archive() ) {
		$file_name = 'archive';
	} elseif ( is_tag() ) {
		$file_name = 'tag';
	} elseif ( is_author() ) {
		$file_name = 'author';
	} elseif ( is_404() ) {
		$file_name = '404';
	} else {
		$file_name = 'all';
	}
	return $file_name;
}

// Ativação do Menu abaixo
add_theme_support( 'menus' );
add_action( 'after_setup_theme', 'open_tema_register_menu' );
function open_tema_register_menu() {
	//Linhas para dicionar Tipo de Menu
	register_nav_menu( 'principal', __( 'Menu Principal', 'open_tema' ) );
	register_nav_menu( 'blog', __( 'Menu Blog', 'open_tema' ) );
	register_nav_menu( 'rodape', __( 'Menu Rodapé', 'open_tema' ) );
}

// Função para Título dinamico
function titulo_dinamico(){
	global $post;
	if ( is_tax() or is_category() or is_tag() ) {
		$titulo = single_term_title();
	} elseif ( is_search() ) {
		$titulo = 'Você está buscando por: '.get_search_query();
	} elseif ( is_archive() ) {
		$titulo = get_the_archive_title();
	} elseif ( is_404() ) {
		$titulo = 'Página 404';
	} else {
		$titulo = the_title();
	}
	return $titulo;
}

// Formulário de Pesquisa
function my_search_form( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" ><input type="text" placeholder="Busque seu imóvel" value="' . get_search_query() . '" name="s" id="s" /><button type="submit"><i class="fas fa-search"></i></button></form>';

	return $form;
}
add_filter( 'get_search_form', 'my_search_form' );

function bbx_search($search_text, $paged){

	$args1 = array(

			'post_type' => 'imoveis',
			'post_status'=> 'publish',
			'paged' => $paged,
			'meta_query' => array(
			'relation' => 'OR',
				array(
					'key' => 'residencial', 'value' => $search_text),
				array(
					'key' => 'sobre_o_imovel','value' => $search_text),
				array(
					'key' => 'titulo_pagina','value' => $search_text),              
				array(
					'key' => 'tags_imovel','value' => $search_text),
				)

		);

			return new WP_Query( $args1 );
	}

// Adicionar Formato do post
add_theme_support( 'post-formats', array( 'aside' , 'gallery' , 'link' , 'image' , 'quote' , 'status' , 'video' , 'audio' , 'chat' ) );

// Adicionar Paginação
if ( ! function_exists( 'post_pagination' ) ) :
   function post_pagination() {
     global $wp_query;
     $pager = 999999999; // need an unlikely integer
        echo paginate_links( array(
             'base' => str_replace( $pager, '%#%', esc_url( get_pagenum_link( $pager ) ) ),
             'format' => '?paged=%#%',
             'current' => max( 1, get_query_var('paged') ),
             'total' => $wp_query->max_num_pages
        ) );
   }
endif;

// Theme options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Opções do tema',
		'menu_title'	=> 'Opções do tema',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'position'      => 1,
		'icon_url'      => 'dashicons-admin-appearance',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Seções do tema',
		'menu_title'	=> 'Seções do tema',
		'parent_slug'	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'position'      => 1,
		'icon_url'      => 'dashicons-admin-appearance',
		'redirect'		=> false
	));
	
}

//ACF MAPS API
function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyD49HoQudhBRU8wucBiTd4HxtgHT2fzTa0';
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyD49HoQudhBRU8wucBiTd4HxtgHT2fzTa0');
}
add_action('acf/init', 'my_acf_init');


//Exclude pages from WordPress Search
if (!is_admin()) {
	function wpb_search_filter($query) {
	if ($query->is_search) {
	$query->set('post_type', 'imoveis');
	}
	return $query;
	}
	add_filter('pre_get_posts','wpb_search_filter');
	}


?>